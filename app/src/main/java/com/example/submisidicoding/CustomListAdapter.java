package com.example.submisidicoding;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

public class CustomListAdapter extends ArrayAdapter {

    private Context context;
    private String[] namanegara;
    private String[] detailnegara;
    private int[] gambarbendera;

    public CustomListAdapter(Context context1, String[] namanegara, String[] detailnegara, int[] gambarbendera) {
        super(context1, R.layout.item_negara, namanegara);
        this.context = context1;
        this.namanegara = namanegara;
        this.detailnegara = detailnegara;
        this.gambarbendera = gambarbendera;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //aturlayout
        View view = LayoutInflater.from(context).inflate(R.layout.item_negara, parent, false);

        //findviewbyid
        TextView tvNamaNegara = view.findViewById(R.id.tv_detail_nama);
        ImageView ivGambarBendera = view.findViewById(R.id.iv_detail_gambar);

        //set data
        tvNamaNegara.setText(namanegara[position]);
        ivGambarBendera.setImageResource(gambarbendera[position]);

        return view;
    }
}