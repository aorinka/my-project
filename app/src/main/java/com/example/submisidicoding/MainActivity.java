package com.example.submisidicoding;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    ListView list;
    String[] namanegara = {
            "Brunei Darussalam",
            "Filipina",
            "Indonesia",
            "Kamboja",
            "Laos",
            "Malaysia",
            "Myanmar",
            "Singapura",
            "Thailand",
            "Vietnam"
    };
    String[] detailnegara = {
            "Brunei Darussalam, adalah negara berdaulat di Asia Tenggara yang terletak di pantai utara pulau Kalimantan. Negara ini memiliki wilayah seluas 5.765 km² yang menempati pulau Borneo dengan garis pantai seluruhnya menyentuh Laut Cina Selatan. Wilayahnya dipisahkan ke dalam dua negara bagian di Malaysia yaitu Sarawak dan Sabah , saat ini, Brunei Darussalam memiliki Indeks Pembangunan Manusia tertinggi kedua di Asia Tenggara setelah Singapura, sehingga diklasifikasikan sebagai negara maju. Menurut Dana Moneter Internasional, Brunei memiliki produk domestik bruto per kapita terbesar kelima di dunia dalam keseimbangan kemampuan berbelanja. Sementara itu, Forbes menempatkan Brunei sebagai negara terkaya kelima dari 182 negara karena memiliki ladang minyak bumi dan gas alam yang luas. Selain itu, Brunei juga terkenal dengan kemakmurannya dan ketegasan dalam melaksanakan syariat Islam, baik dalam bidang pemerintahan maupun kehidupan bermasyarakat.",
            "Filipina atau Republik Filipina (bahasa Tagalog: Republika ng Pilipinas) adalah sebuah negara republik di Asia Tenggara, sebelah utara Indonesia, dan Malaysia. Filipina merupakan sebuah negara kepulauan yang terletak di Lingkar Pasifik Barat, negara ini terdiri dari 7.641 pulau. Selama ribuan tahun, warga kepulauan Filipina, dan pekerja keras ini telah mengembangkan sistem cocok tanam Padi yang sangat maju, yang menyediakan makanan pokok bagi masyarakatnya. Filipina adalah negara paling maju di Benua Asia setelah Perang Dunia II, namun sejak saat itu telah tertinggal di belakang negara-negara lain akibat pertumbuhan ekonomi yang lemah, penyitaan kekayaan yang dilakukan pemerintah, korupsi yang luas, dan pengaruh-pengaruh neo-kolonial. Meskipun begitu, saat ini Filipina mengalami pertumbuhan ekonomi yang moderat, yang banyak disumbangkan dari pengiriman uang oleh pekerja-pekerja Filipina di luar negeri, dan sektor teknologi informasi yang sedang tumbuh pesat. Filipina seringkali dianggap sebagai satu-satunya negara di Benua Asia di mana pengaruh budaya Barat terasa sangat kuat.",
            "Republik Indonesia (RI) atau Negara Kesatuan Republik Indonesia (NKRI), atau lebih umum disebut Indonesia, adalah negara di Asia Tenggara yang dilintasi garis khatulistiwa dan berada di antara daratan benua Asia dan Australia, serta antara Samudra Pasifik dan Samudra Hindia. Indonesia adalah negara kepulauan terbesar di dunia yang terdiri dari 17.504 pulau. Nama alternatif yang biasa dipakai adalah Nusantara.Dengan populasi Hampir 270.054.853 jiwa pada tahun 2018, Indonesia adalah negara berpenduduk terbesar keempat di dunia dan negara yang berpenduduk Muslim terbesar di dunia, dengan lebih dari 230 juta jiwa. Bentuk negara Indonesia adalah negara kesatuan dan bentuk pemerintahan Indonesia adalah republik, dengan Dewan Perwakilan Rakyat, Dewan Perwakilan Daerah dan Presiden yang dipilih secara langsung. Ibu kota negara Indonesia adalah Jakarta. ",
            "Kerajaan Kamboja adalah sebuah negara berbentuk monarki konstitusional di Asia Tenggara. Negara ini merupakan penerus Kekaisaran Khmer yang pernah menguasai seluruh Semenanjung Indochina antara abad ke-11 dan 14.Kamboja berbatasan dengan Thailand di sebelah barat, Laos di utara, Vietnam di timur, dan Teluk Thailand di selatan. Sungai Mekong dan Danau Tonle Sap melintasi negara ini.Menjelang kemerdekaannya, Negara Kesatuan Republik Indonesia banyak membantu negara Kamboja ini. Buku - buku taktik perang karangan perwira militer Indonesia banyak digunakan oleh militer Kamboja. Oleh karenanya, para calon perwira di militer Kamboja, wajib belajar dan dapat berbahasa Indonesia.",
            "Republik Demokratik Rakyat Laos adalah negara yang terkurung daratan di Asia Tenggara, berbatasan dengan Myanmar dan Republik Rakyat Tiongkok di sebelah barat laut, Vietnam di timur, Kamboja di selatan, dan Thailand di sebelah barat. Dari abad ke-14 hingga abad ke-18, negara ini disebut Lan Xang atau. Negeri Seribu Gajah. Laos adalah sebuah Negara Republik yang dikelilingi oleh daratan dan terletak di bagian utara Semenanjung Indochina. Laos berasal dari kata Lan Xang yang artinya kerajaan gajah. Negara ini adalah satu-satunya Negara di kawasan Asia Tenggara yang tidak memiliki pantai. Laos pernah dijajah oleh Prancis dan memperoleh kemerdekaan pada 22 Oktober 1953 dalam bentuk kerajaan. Sejak 2 Desember 1975 kerajan Laos berubah menjadi Republik Laos. Laos adalah salah satu negara komunis dengan kepala pemerintahan berupa presiden yang bernama Choummaly Sayasone dan dibantu oleh perdana menteri yang bernama Bouasone Bouphavanh. Jika dilihat dari sudut pandang Geografi Politik, letak wilayah negara Laos yang tidak memiliki wilayah laut atau pantai dikenal dengan sebutan kawasan land-lock. ",
            "Malaysia adalah sebuah negara federal yang terdiri dari tiga belas negeri (negara bagian) dan tiga wilayah federal di Asia Tenggara dengan luas 329.847 km persegi. Ibu kotanya adalah Kuala Lumpur, sedangkan Putrajaya menjadi pusat pemerintahan federal. Jumlah penduduk negara ini mencapai 30.697.000 jiwa pada tahun 2015. Negara ini dipisahkan ke dalam dua kawasan — Malaysia Barat dan Malaysia Timur — oleh Kepulauan Natuna, wilayah Indonesia di Laut Tiongkok Selatan. Malaysia berbatasan dengan Thailand, Indonesia, Singapura, Brunei, dan Filipina. Negara ini terletak di dekat khatulistiwa dan beriklim tropika. Kepala negara Malaysia adalah seorang Raja atau seorang Sultan yang dipilih secara bergiliran setiap 5 tahun sekali, hanya negeri-negeri (negara bagian) yang diperintah oleh Raja/Sultan saja yang diperbolehkan mengirimkan wakilnya untuk menjadi Raja Malaysia. Raja Malaysia biasanya memakai gelar Sri Paduka Baginda Yang di-Pertuan Agong, dan pemerintahannya dikepalai oleh seorang Perdana Menteri. Model pemerintahan Malaysia mirip dengan sistem parlementer Westminster.",
            "Republik Persatuan Myanmar adalah sebuah negara berdaulat di Asia Tenggara. Myanmar berbatasan dengan India dan Bangladesh di sebelah barat, Thailand dan Laos di sebelah timur dan China di sebelah utara dan timur laut. Negara seluas 676.578 km² ini telah diperintah oleh pemerintahan militer sejak kudeta tahun 1988. Negara ini adalah negara berkembang dan memiliki populasi lebih dari 51 juta jiwa (sensus 2014). Ibu kota negara ini sebelumnya terletak di Yangon sebelum dipindahkan oleh pemerintahan junta militer ke Naypyidaw pada tanggal 7 November 2005. Myanmar telah bergabung sebagai anggota Perhimpunan Bangsa-Bangsa Asia Tenggara (ASEAN) sejak tahun 1997.",
            "Singapura adalah sebuah negara pulau di lepas ujung selatan Semenanjung Malaya, 137 kilometer (85 mi) di utara khatulistiwa di Asia Tenggara. Negara ini terpisah dari Malaysia oleh Selat Johor di utara, dan dari Kepulauan Riau, Indonesia oleh Selat Singapura di selatan. Singapura adalah pusat keuangan terdepan ketiga di dunia dan sebuah kota dunia kosmopolitan yang memainkan peran penting dalam perdagangan dan keuangan internasional. Pelabuhan Singapura adalah satu dari lima pelabuhan tersibuk di dunia.Singapura memiliki sejarah imigrasi yang panjang. Penduduknya yang beragam berjumlah kira-kira 6 juta jiwa, terdiri dari Cina, Melayu, India, Arab, berbagai keturunan Asia, dan Kaukasoid 42% penduduk Singapura adalah orang asing yang bekerja dan menuntut ilmu di sana. Pekerja asing membentuk 50% dari sektor jasa. Negara ini adalah yang terpadat kedua di dunia setelah Monako. A.T. Kearney menyebut Singapura sebagai negara paling terglobalisasi di dunia dalam Indeks Globalisasi tahun 2006.Sebelum merdeka tahun 1965, Singapura adalah pelabuhan dagang yang beragam dengan PDB per kapita $511, tertinggi ketiga di Asia Timur pada saat itu. Setelah merdeka, investasi asing langsung dan usaha pemerintah untuk industrialisasi berdasarkan rencana bekas Deputi Perdana Menteri Dr. Goh Keng Swee membentuk ekonomi Singapura saat ini.",
            "Kerajaan Thai yang lebih sering disebut Thailand dalam bahasa Inggris, atau dalam bahasa aslinya Mueang Thai (adalah sebuah negara di Asia Tenggara yang berbatasan dengan Laos dan Kamboja di timur, Malaysia dan Teluk Siam di selatan, dan Myanmar dan Laut Andaman di barat. Kerajaan Thai dahulu dikenal sebagai Siam sampai tanggal 11 Mei 1949. Kata Thai berarti kebebasan dalam bahasa Thai, tetapi juga dapat merujuk kepada suku Thai, sehingga menyebabkan nama Siam masih digunakan di kalangan warga negara Thai terutama kaum minoritas Tionghoa dan Amerika.",
            "Vietnam bernama resmi Republik Sosialis adalah negara paling timur di Semenanjung Indochina di Asia Tenggara. Vietnam berbatasan dengan Republik Rakyat Tiongkok di sebelah utara, Laos di sebelah barat laut, Kamboja di sebelah barat daya dan di sebelah timur terbentang Laut China Selatan. Dengan populasi sekitar 84 juta jiwa, Vietnam adalah negara terpadat nomor 13 di dunia. Vietnam termasuk di dalam grup ekonomi  menurut pemerintah, GDP Vietnam tumbuh sebesar 8.17% pada tahun 2006, negara dengan pertumbuhan tercepat kedua di Asia Timur dan pertama di Asia Tenggara. Pada akhir tahun 2007, menteri keuangan menyatakan pertumbuhan GDP Vietnam diperkirakan mencapai rekor tertinggi dalam sepuluh tahun terakhir sebesar 8.44%."
    };

    int[] gambarbendera = {
            R.drawable.brunei,
            R.drawable.filipina,
            R.drawable.indonesia,
            R.drawable.kamboja,
            R.drawable.laos,
            R.drawable.malaysia,
            R.drawable.myanmar,
            R.drawable.singapura,
            R.drawable.thailand,
            R.drawable.vietnam

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list = findViewById(R.id.list_view);
        //ArrayAdapter adapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_expandable_list_item_1);
        CustomListAdapter adapter = new CustomListAdapter(MainActivity.this, namanegara, detailnegara, gambarbendera);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                Intent pindah = new Intent(MainActivity.this, Detail.class);
                pindah.putExtra(Konstanta.DATANAMA, namanegara[position]);
                pindah.putExtra(Konstanta.DATADETAIL, detailnegara[position]);
                pindah.putExtra(Konstanta.DATAGAMBAR, gambarbendera[position]);
                startActivity(pindah);

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_Profile) {
            startActivity(new Intent(this, Profile.class));
        }
        return true;
    }
}
