package com.example.submisidicoding;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

public class Detail extends AppCompatActivity {
    private static final String TAG = "DetailActvity";

    TextView tvnamanegara;
    TextView tvdetailnegara;
    ImageView ivgambarbendera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        String namanegara = getIntent().getStringExtra(Konstanta.DATANAMA);
        String detailnegara = getIntent().getStringExtra(Konstanta.DATADETAIL);
        int gambarbendera = getIntent().getIntExtra(Konstanta.DATAGAMBAR , 0);

        //log
        Log.d(TAG, "Nama : " + namanegara);
        Log.d(TAG, "Detail : " + detailnegara);
        Log.d(TAG, "Gambar : " + gambarbendera);

        tvnamanegara = findViewById(R.id.tv_detail_nama);
        tvdetailnegara = findViewById(R.id.tv_detail_negara);
        ivgambarbendera = findViewById(R.id.iv_detail_gambar);

        tvnamanegara.setText(namanegara);
        tvdetailnegara.setText(detailnegara);
        ivgambarbendera.setImageResource(gambarbendera);
    }
}
